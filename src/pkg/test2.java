package pkg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import org.testng.annotations.Test;
public class test2 {
	WebDriver driver = new FirefoxDriver();
	@BeforeTest
	   public void launchapp() {
	      // Puts an Implicit wait, Will wait for 10 seconds before throwing exception
		      driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		      
		      // Launch website
		      driver.navigate().to("https://www.cleartrip.com");
		      driver.manage().window().maximize();
	   }
	@Test
	   public void Search() {
		driver.findElement(By.xpath(".//*[@id='SearchForm']/nav/ul/li[2]/label/strong")).click();
		driver.findElement(By.xpath(".//*[@id='FromTag']")).sendKeys("Bangalore, IN - Kempegowda International Airport (BLR)");
		

		driver.findElement(By.xpath(".//*[@id='ToTag']")).sendKeys("Ahmedabad, IN - Sardar Vallabh Bhai Patel (AMD)");
		
		driver.findElement(By.xpath(".//*[@id='DepartDate']")).clear();
		driver.findElement(By.xpath(".//*[@id='DepartDate']")).sendKeys("Wed, 1 Nov, 2017");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='ReturnDate']")).clear();
		
		driver.findElement(By.xpath(".//*[@id='ReturnDate']")).sendKeys("Wed, 2 Nov, 2017");
		
		driver.findElement(By.xpath(".//*[@id='ui-datepicker-div']/div[1]/table/tbody/tr[1]/td[4]/a")).click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.xpath(".//*[@id='Adults']")).sendKeys("2");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	
		driver.findElement(By.xpath(".//*[@id='SearchBtn']")).click();
		
	}
	@AfterTest
	   public void terminatetest() throws InterruptedException {
		
			
			driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			Thread.sleep(10000);
			driver.close();
		}

}
