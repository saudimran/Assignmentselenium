package pkg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;



public class test {

	public static void main(String[] args) throws InterruptedException {
		
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.cleartrip.com");
		
		driver.findElement(By.xpath(".//*[@id='SearchForm']/nav/ul/li[2]/label/strong")).click();
		driver.findElement(By.xpath(".//*[@id='FromTag']")).sendKeys("Bangalore, IN - Kempegowda International Airport (BLR)");
		

		driver.findElement(By.xpath(".//*[@id='ToTag']")).sendKeys("Ahmedabad, IN - Sardar Vallabh Bhai Patel (AMD)");
		
		driver.findElement(By.xpath(".//*[@id='DepartDate']")).clear();
		driver.findElement(By.xpath(".//*[@id='DepartDate']")).sendKeys("Wed, 1 Nov, 2017");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='ReturnDate']")).clear();
		
		driver.findElement(By.xpath(".//*[@id='ReturnDate']")).sendKeys("Wed, 2 Nov, 2017");
		
		driver.findElement(By.xpath(".//*[@id='ui-datepicker-div']/div[1]/table/tbody/tr[1]/td[4]/a")).click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.xpath(".//*[@id='Adults']")).sendKeys("2");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	
		driver.findElement(By.xpath(".//*[@id='SearchBtn']")).click();
		
		}

}
